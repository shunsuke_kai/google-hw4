import static org.junit.Assert.*;

import org.junit.Test;


public class Hw4Test {

	@Test
	public void ltest() {    //rotate left one by one
		String w0="apple",w1="pplea",w2="pleap",w3="leapp",w4="eappl",w5="apple";
		assertEquals(w0,Hw4.rotate("apple",0));
		assertEquals(w1,Hw4.rotate("apple",1));
		assertEquals(w2,Hw4.rotate("apple",2));
		assertEquals(w3,Hw4.rotate("apple",3));
		assertEquals(w4,Hw4.rotate("apple",4));
		assertEquals(w5,Hw4.rotate("apple",5));
		
	}
	public void rtest() {    //rotate left one by one
		String w0="apple",w1="eappl",w2="leapp",w3="pleap",w4="pplea",w5="apple";
		assertEquals(w0,Hw4.rotate("apple",0));
		assertEquals(w1,Hw4.rotate("apple",-1));
		assertEquals(w2,Hw4.rotate("apple",-2));
		assertEquals(w3,Hw4.rotate("apple",-3));
		assertEquals(w4,Hw4.rotate("apple",-4));
		assertEquals(w5,Hw4.rotate("apple",-5));
		
	}

}
