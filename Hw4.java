import java.util.Scanner;
public class Hw4 {
	public static void main (String args[]){
		String word="apple";
		
		System.out.println(rotate(word,9));
		
	}
	
	public static String rotate(String word,int n){
		if(n>0){
			n%=word.length();        //(mod length of word)
			for(int i=0;i<n;i++){
				word=roll_l(word);
			}
		}else{
			n*=-1;
			n%=word.length();       //(mod length of word)
			for(int i=0;i<n;i++){
				word=roll_r(word);
			}
		}
		
		return word;
	}
	
	public static String roll_l(String word){   //one rotate right
		char c[]=word.toCharArray();
		char tmp=c[0];
		for(int i=0;i<c.length-1;i++){
			c[i]=c[i+1];
		}
		c[c.length-1]=tmp;
		word= new String(c);
		return word;
	}
	public static String roll_r(String word){   //one rotate left
		char c[]=word.toCharArray();
		char tmp=c[c.length-1];
		
		for(int i=c.length-1;i>0;i--){
			c[i]=c[i-1];
		}
		c[0]=tmp;
		word=new String(c);
		return word;
	}
	
	
	
	
}
